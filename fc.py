import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

train_data = pd.read_csv('./train.csv')
X = train_data.iloc[:, 1:-1]
y = train_data.loc[:, 'Cover_Type']

train_X, val_X, train_y, val_y = train_test_split(X, y, )

rfc = RandomForestClassifier(n_estimators=200, n_jobs=-1)

rfc.fit(train_X, train_y)
pred_y = rfc.predict(val_X)
acc = accuracy_score(val_y, pred_y)
print(f'Random Forest: Acc {acc:.3f}')

test_data = pd.read_csv('./test.csv')
predictions = rfc.predict(test_data.iloc[:, 1:])

results_table = pd.DataFrame({'Id': test_data.Id, 'Cover_Type': predictions})
results_table.to_csv('submission.csv', index=False)
